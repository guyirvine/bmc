#!/bin/bash

source ~/.env

FILE_NAME=bmc.sql
COMPRESSED_FILE_NAME=bmc.$(date +%Y%m%d%H%M).tar.bz2
WORKING_DIR=/home/bmc/backup

mkdir -p $WORKING_DIR

find "$WORKING_DIR" -iname 'bmc.*.tar.bz2' -type f -mtime +6 -delete

pg_dump bmc >> $WORKING_DIR/bmc.sql || exit 1

tar -jcf "$WORKING_DIR/$COMPRESSED_FILE_NAME" -C "$WORKING_DIR" bmc.sql || exit 2
rm $WORKING_DIR/bmc.sql || exit 3

rm -f "$WORKING_DIR/$FILE_NAME.latest.tar.bz2" || exit 4
ln -s "$WORKING_DIR/$COMPRESSED_FILE_NAME" "$WORKING_DIR/$FILE_NAME.latest.tar.bz2" || exit 5
