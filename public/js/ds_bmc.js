/*jslint browser: true*/
/*jslint nomen: true */
/*jslint node: true */

/*global $, _, alert, console, app, AWS, ds_aws */

"use strict";

var ds_bmc = {};

ds_bmc.get = function (id, callback) {
    $.getJSON('/bmc/' + id, function (obj) {
        callback(obj);
    });
};

ds_bmc.get_all = function (callback) {
    $.getJSON('/bmc', function (l) {
        callback(l);
    });
};

ds_bmc.save = function (payload, callback) {
    $.post('/bmc/' + payload.id, JSON.stringify(payload), function (id) {
        callback(id);
    });
};

ds_bmc.save_section = function (id, name, value, callback) {
    $.post('/bmc/' + id + '/' + name, value, function () {
        callback();
    });
};

ds_bmc.save_details = function (id, name, designedfor, designedby, bmc_when, iteration, callback) {
    var payload = {
        name: name,
        designedfor: designedfor,
        designedby: designedby,
        bmc_when: bmc_when,
        iteration: iteration
    };
    $.post('/bmc/' + id + '/details', JSON.stringify(payload), function () {
        callback();
    });
};
