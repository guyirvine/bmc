/*jslint browser: true*/
/*jslint nomen: true */
/*jslint node: true */

/*global $, _, alert, console, app, AWS, ds_aws */

"use strict";

var ds_assumptions = {};

ds_assumptions.get = function (id, callback) {
    $.getJSON('/assumptions/' + id, function (obj) {
        callback(obj);
    });
};

ds_assumptions.get_all = function (callback) {
    $.getJSON('/assumptions', function (l) {
        callback(l);
    });
};

ds_assumptions.save = function (payload, callback) {
    $.post('/assumptions/' + payload.id, JSON.stringify(payload), function (id) {
        callback(id);
    });
};

ds_assumptions.save_section = function (id, name, value, callback) {
    $.post('/assumptions/' + id + '/' + name, value, function () {
        callback();
    });
};

ds_assumptions.save_details = function (id, name, designedfor, designedby, assumptions_when, iteration, callback) {
    var payload = {
        name: name,
        designedfor: designedfor,
        designedby: designedby,
        assumptions_when: assumptions_when,
        iteration: iteration
    };
    $.post('/assumptions/' + id + '/details', JSON.stringify(payload), function () {
        callback();
    });
};
