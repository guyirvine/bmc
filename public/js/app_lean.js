/*jslint browser: true*/
/*jslint nomen: true */
/*jslint node: true */

/*global $, _, jQuery, alert, console, ko, moment, UUIDjs, gapi, AWS, guid, ds_aws, ds, c3, d3, Trello, app, ds_lean, Vue */

"use strict";

var app_lean = {};

app_lean.lean_section_names = ['problem', 'solution', 'keymetrics',
                                  'uniquevalueproposition', 'unfairadvantage',
                                  'customersegments', 'channels',
                                  'coststructure', 'revenuestreams'];

app_lean.show_openlean = function () {
    var opencanvas;

    opencanvas = $('.templates .opencanvas').clone();
    opencanvas.find('header').text('Go to a Lean Canvas');
    opencanvas.find('a.cancel').prop('href', '#lean');
    opencanvas.find('footer a.new').prop('href', '#newlean');

    ds_lean.get_all(function (l) {
        console.log('ds_lean.get_all.1 ', l);
        var ul, li;
        ul = opencanvas.find('ul');
        li = ul.find('li').remove();
        _.each(l, function (el) {
            var _li = li.clone();
            _li.find('a').text(el.name);
            _li.find('a').attr('href', '#loadlean+' + el.id);
            ul.append(_li);
        });
    });

    $('.popup').empty();
    $('.popup').append(opencanvas);
    $('.popup').removeClass('hide');
};

app_lean.create_lean = function () {
    $.post('/lean', 'New Lean Canvas', function (id) {
        console.log('app_lean.create_lean.1.1 ', id);
        window.location.hash = '#loadlean+' + id;
    });
};

app_lean.load_lean = function (id) {
    console.log('app_lean.load_lean.1 ', id);
    app_lean.lean_id = id;

    ds_lean.get(id, function (obj) {
        localStorage.lastroute = 'loadlean+' + id;
        console.log('app_lean.load_lean.1 obj: ', obj);

        _.each(app_lean.lean_section_names, function (section_name) {
            if (obj[section_name] !== '') {
                obj[section_name] = obj[section_name].split('\n');
            }
        });

        app_lean.lean = obj;
        window.location.hash = "#lean";
    });
};

app_lean.show_lean = function () {
    console.log('app_lean.show_lean.1 ');
    $('body').removeClass('vpc');
    $('body').addClass('lean');
    app.return_to_hash = 'lean';

    var lean = $('.templates .lean').clone();
    _.each(app_lean.lean_section_names, function (section_name) {
        console.log('app_lean.show_lean.1 ', section_name);
        var ul = $('.templates .section-li ul').clone();

        ul.find('li').attr('v-for', 'string in lean.' + section_name);

        lean.find('.canvas .' + section_name)
            .append(ul)
            .attr('v-on:click', 'show_section("' + section_name + '")');
    });

    lean.find('.details').attr('v-on:click', 'show_editdetails');

    $('.container1').empty();
    $('.container1').append(lean);

    app_lean.lean_vue = new Vue({
        el: '.container1',
        data: { 'lean': app_lean.lean },
        methods: {
            show_section: function (section_name) {
                window.location.hash = 'leansection+' + section_name;
            },
            show_editdetails: function () {
                window.location.hash = "leaneditdetails";
            }
        }
    });
};

app_lean.show_lean_section = function (name) {
    console.log('app_lean.show_lean_section.1 ', name);
    var textinput, txt;

    textinput = $('.templates .textinput').clone();
    txt = (app_lean.lean[name] === '') ? '' : app_lean.lean[name].join('\n');

    textinput.find('header').text($('.container1 .lean .canvas .' + name + ' h2').text());
    textinput.find('.tips').append($('.templates .leantips .' + name).clone());
    textinput.find('textarea').val(txt);
    textinput.find('.cancel').attr('href', '#lean');

    textinput.find('.update').on('click', function () {
        var list = textinput.find('textarea').val().split("\n");
        app_lean.lean[name] = _.filter(list, function (el) { return el !== ""; });
        ds_lean.save_section(app_lean.lean.id, name, app_lean.lean[name].join('\n'), function () {
            console.log('app_lean.show_lean_section.1 ', name, app_lean.lean[name].join('\n'));
            window.location.hash = 'lean';
        });
    });

    $('.popup').empty();
    $('.popup').append(textinput);
    $('.popup').removeClass('hide');
    textinput.find('textarea').focus();
};

app_lean.show_editdetails = function () {
    var editdetails;

    editdetails = $('.templates .leaneditdetails').clone();

    $('.popup').empty();
    $('.popup').append(editdetails);
    $('.popup').removeClass('hide');

    app_lean.v = new Vue({
        el: '.popup .editdetails',
        data: { 'lean': app_lean.lean },
        methods: {
            save: function () {
                ds_lean.save_details(app_lean.lean.id,
                                    app_lean.lean.name,
                                    app_lean.lean.designedfor,
                                    app_lean.lean.designedby,
                                    app_lean.lean.lean_when,
                                    app_lean.lean.iteration,
                                    function () {
                        window.location.hash = 'lean';
                    });
            }
        }
    });

    editdetails.find('textarea.designedfor').focus();
};
