/*jslint browser: true*/
/*jslint nomen: true */
/*jslint node: true */

/*global $, _, jQuery, alert, console, ko, moment, UUIDjs, gapi, AWS, ds_aws, ds, c3, d3, Trello, app, app_bmc, app_vpc, ds_bmc_aws, Vue, app_lean, app_leanux, app_persona, app_assumptions */

"use strict";

var app = {};

app.show_view = function (hash) {
    var routes, hashParts, viewFn;

    $('.popup').addClass('hide');

    routes = {
        '#showlogin': app.show_login,
        '#setpassword': app.show_setpassword,
        '#bmceditdetails': app_bmc.show_editdetails,
        '#vpceditdetails': app_vpc.show_editdetails,
        '#leaneditdetails': app_lean.show_editdetails,
        '#leanuxeditdetails': app_leanux.show_editdetails,
        '#personaeditdetails': app_persona.show_editdetails,
        '#assumptionseditdetails': app_assumptions.show_editdetails,
        '#journeyseditdetails': app_journeys.show_editdetails,
        '#questionseditdetails': app_questions.show_editdetails,
        '#bmcsection': app_bmc.show_bmc_section,
        '#vpcsection': app_vpc.show_vpc_section,
        '#leansection': app_lean.show_lean_section,
        '#leanuxsection': app_leanux.show_leanux_section,
        '#personasection': app_persona.show_persona_section,
        '#assumptionssection': app_assumptions.show_assumptions_section,
        '#journeyssection': app_journeys.show_journeys_section,
        '#questionssection': app_questions.show_questions_section,
        '#bmc': app_bmc.show_bmc,
        '#vpc': app_vpc.show_vpc,
        '#lean': app_lean.show_lean,
        '#leanux': app_leanux.show_leanux,
        '#persona': app_persona.show_persona,
        '#assumptions': app_assumptions.show_assumptions,
        '#journeys': app_journeys.show_journeys,
        '#questions': app_questions.show_questions,
        '#loadbmc': app_bmc.load_bmc,
        '#loadvpc': app_vpc.load_vpc,
        '#loadlean': app_lean.load_lean,
        '#loadleanux': app_leanux.load_leanux,
        '#loadpersona': app_persona.load_persona,
        '#loadassumptions': app_assumptions.load_assumptions,
        '#loadjourneys': app_journeys.load_journeys,
        '#loadquestions': app_questions.load_questions,
        '#newcanvas': app.show_newcanvas,
        '#opencanvas': app.show_opencanvas,
        '#openbmc': app_bmc.show_openbmc,
        '#openvpc': app_vpc.show_openvpc,
        '#openlean': app_lean.show_openlean,
        '#openleanux': app_leanux.show_openleanux,
        '#openpersona': app_persona.show_openpersona,
        '#openassumptions': app_assumptions.show_openassumptions,
        '#openjourneys': app_journeys.show_openjourneys,
        '#openquestions': app_questions.show_openquestions,
        '#newbmc': app_bmc.create_bmc,
        '#newvpc': app_vpc.create_vpc,
        '#newlean': app_lean.create_lean,
        '#newleanux': app_leanux.create_leanux,
        '#newpersona': app_persona.create_persona,
        '#newassumptions': app_assumptions.create_assumptions,
        '#newjourneys': app_journeys.create_journeys,
        '#newquestions': app_questions.create_questions,
        '#logout': app.logout,
        '#cancel': app.cancel
    };

    hashParts = hash.split('+');
    hashParts[1] = hashParts.slice(1).join('+');
    viewFn = routes[hashParts[0]];
    console.log("viewFn: ", hashParts[0], hashParts[1]);
    if (viewFn) {
        viewFn(hashParts[1]);
    }
};

app.return_to_hash = 'bmc';

app.cancel = function () {
    window.location.hash = app.return_to_hash;
};

app.show_newcanvas = function () {
    var r;

    console.log('app.show_newcanvas.1');

    r = $('.templates .newcanvas').clone();
    r.find('a.cancel').prop('href', '#cancel');

    $('.popup').empty();
    $('.popup').append(r);
    $('.popup').removeClass('hide');
};

app.show_opencanvas = function () {
    var r;

    console.log('app.show_opencanvas.1');

    r = $('.templates .opencanvas').clone();
    r.find('header').text('Open Canvas');
    r.find('a.cancel').prop('href', '#cancel');

    $.getJSON('/canvas', function (l) {
        console.log('app.show_opencanvas.1 ', l);

        var ul, li;
        ul = r.find('ul');
        li = ul.find('li').remove();
        _.each(l, function (el) {
            var _li, hash;
            _li = li.clone();
            _li.find('a').text(el.label);

            hash = '#load' + el.type + '+' + el.id;
            _li.find('a').attr('href', hash);
            ul.append(_li);
        });
    });


    $('.popup').empty();
    $('.popup').append(r);
    $('.popup').removeClass('hide');
};




app.login = function () {
    $('body').addClass('loggedin');
    if (localStorage.lastroute === undefined) {
        window.location.hash = 'openbmc';
    } else {
        window.location.hash = localStorage.lastroute;
    }
};

app.logout = function () {
    $.ajax({
        url: '/session',
        type: 'DELETE'
    });

    $.cookie("session_key", '', -1);
    $('.container1').empty();
    window.location.hash = 'showlogin';
};

app.check_password = function (username, password, callback) {
    console.log('app.check_password.1 ', username, password);
    $.post('/session', JSON.stringify({ username: username, password: password }), function (data) {
        console.log('app.check_password.1.1 ', data);
        $.cookie("session_key", data.session_key);
        callback(true);
    }, 'json')
        .error(function (data) {
            console.log('app.check_password.1.2 ', data);
            callback(false);
        });
};

app.show_login = function () {
    console.log('app.show_login.1 ');
    var r = $('.templates .login').clone();

    r.find('.loginbutton').on('click', function () {
        app.check_password(r.find('.username').val(), r.find('.password').val(), function (successful) {
            console.log('app.login_view.1 ', successful);
            if (successful === true) {
                window.location.hash = 'login';
            } else {
                r.find('.note').text('Username / Password combination not recognised');
            }
        });
        return false;
    });

    $('.popup').empty();
    $('.popup').append(r);
    $('.popup').removeClass('hide');
    $('.popup .name').focus();
    console.log('app.show_login.2 ');
};

app.logout = function () {
    $.ajax({
        url: '/session',
        type: 'DELETE'
    });

    $.cookie("session_key", '', -1);
    $('.container').empty();
    window.location.hash = 'showlogin';
};

app.valid_session = function (callback) {
    console.log('app.valid_session.1 ');
    if ($.cookie("session_key") === undefined) {
        console.log('app.valid_session.1.1 ');
        callback(false);
    } else {
        console.log('app.valid_session.1.2 ');
        $.get('/user', function () {
            console.log('app.valid_session.1.2.1 ');
            callback(true);
        }, 'json')
            .error(function (data) {
                console.log('app.valid_session.1.2.2 ', data);
                callback(false);
            });
    }
};

app.show_setpassword = function () {
    console.log('app.show_setpassword.1 ');
    var r = $('.templates .setpassword').clone();

    r.find('.update').on('click', function () {
        if (r.find('.passwordnew').val() !== r.find('.passwordconfirm').val()) {
            r.find('.note').text("Password's don't match.");
            return false;
        }

        var payload = {
                'old': r.find('.passowrdcurrent').val(),
                'new': r.find('.passwordnew').val()
            };
        $.post('/password', JSON.stringify(payload), function (data) {
            console.log('app.show_setpassword.1.1 ', data);
            window.location.hash = 'login';
        })
            .error(function (obj) {
                console.log('app.show_setpassword.1.2 ', obj);
                r.find('.note').text('Current Password incorrect.');
            });

        return false;
    });

    $('.popup').empty();
    $('.popup').append(r);
    $('.popup').removeClass('hide');
    $('.popup .passowrdcurrent').focus();
    console.log('app.show_setpassword.2 ');
};

app.init = function () {
    console.log('app.init.1 ');
    app.valid_session(function (state) {
        console.log('app.init.1.1 ', state);
        if (state === false) {
            console.log('app.init.1.1.1 ');
            window.location.hash = 'showlogin';
        } else {
            console.log('app.init.1.1.2 ');
            app.login();
        }
    });
};

app.apponready = function () {
    window.location.hash = "";
    app.init();
    window.onhashchange = function () {
        app.show_view(window.location.hash);
    };
};
