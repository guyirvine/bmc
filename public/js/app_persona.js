/*jslint browser: true*/
/*jslint nomen: true */
/*jslint node: true */

/*global $, _, jQuery, alert, console, ko, moment, UUIDjs, gapi, AWS, guid, ds_aws, ds, c3, d3, Trello, app, ds_lean, Vue, ds_persona */

"use strict";

var app_persona = {};

app_persona.persona_section_names = ['whoarethey', 'tasksandbehaviours', 'problemsandneeds',
                                    'solutions'];

app_persona.show_openpersona = function () {
    var opencanvas;

    opencanvas = $('.templates .opencanvas').clone();
    opencanvas.find('header').text('Go to a Persona Canvas');
    opencanvas.find('a.cancel').prop('href', '#lean');
    opencanvas.find('footer a.new').prop('href', '#newlean');

    ds_persona.get_all(function (l) {
        console.log('ds_persona.get_all.1 ', l);
        var ul, li;
        ul = opencanvas.find('ul');
        li = ul.find('li').remove();
        _.each(l, function (el) {
            var _li = li.clone();
            _li.find('a').text(el.name);
            _li.find('a').attr('href', '#loadlean+' + el.id);
            ul.append(_li);
        });
    });

    $('.popup').empty();
    $('.popup').append(opencanvas);
    $('.popup').removeClass('hide');
};

app_persona.create_persona = function () {
    $.post('/persona', 'New Persona Canvas', function (id) {
        console.log('app_persona.create_persona.1.1 ', id);
        window.location.hash = '#loadpersona+' + id;
    });
};

app_persona.load_persona = function (id) {
    console.log('app_persona.load_persona.1 ', id);
    app_persona.persona_id = id;

    ds_persona.get(id, function (obj) {
        localStorage.lastroute = 'loadpersona+' + id;
        console.log('app_persona.load_persona.1 obj: ', obj);

        _.each(app_persona.persona_section_names, function (section_name) {
            if (obj[section_name] !== '') {
                obj[section_name] = obj[section_name].split('\n');
            }
        });

        app_persona.persona = obj;
        window.location.hash = "#persona";
    });
};

app_persona.show_persona = function () {
    console.log('app_persona.show_persona.1 ');
    $('body').removeClass('vpc');
    $('body').addClass('persona');
    app.return_to_hash = 'persona';

    var persona = $('.templates .persona').clone();
    _.each(app_persona.persona_section_names, function (section_name) {
        console.log('app_persona.show_persona.1 ', section_name);
        var ul = $('.templates .section-li ul').clone();

        ul.find('li').attr('v-for', 'string in persona.' + section_name);

        persona.find('.canvas .' + section_name)
            .append(ul)
            .attr('v-on:click', 'show_section("' + section_name + '")');
    });

    persona.find('.details').attr('v-on:click', 'show_editdetails');

    $('.container1').empty();
    $('.container1').append(persona);

    app_persona.persona_vue = new Vue({
        el: '.container1',
        data: { 'persona': app_persona.persona },
        methods: {
            show_section: function (section_name) {
                window.location.hash = 'personasection+' + section_name;
            },
            show_editdetails: function () {
                window.location.hash = "personaeditdetails";
            }
        }
    });
};

app_persona.show_persona_section = function (name) {
    console.log('app_persona.show_persona_section.1 ', name);
    var textinput, txt;

    textinput = $('.templates .textinput').clone();
    txt = (app_persona.persona[name] === '') ? '' : app_persona.persona[name].join('\n');

    textinput.find('header').text($('.container1 .persona .canvas .' + name + ' h2').text());
    textinput.find('.tips').append($('.templates .leantips .' + name).clone());
    textinput.find('textarea').val(txt);
    textinput.find('.cancel').attr('href', '#persona');

    textinput.find('.update').on('click', function () {
        var list = textinput.find('textarea').val().split("\n");
        app_persona.persona[name] = _.filter(list, function (el) { return el !== ""; });
        ds_persona.save_section(app_persona.persona.id, name, app_persona.persona[name].join('\n'), function () {
            console.log('app_persona.show_persona_section.1 ', name, app_persona.persona[name].join('\n'));
            window.location.hash = 'persona';
        });
    });

    $('.popup').empty();
    $('.popup').append(textinput);
    $('.popup').removeClass('hide');
    textinput.find('textarea').focus();
};

app_persona.show_editdetails = function () {
    var editdetails;

    editdetails = $('.templates .personaeditdetails').clone();

    $('.popup').empty();
    $('.popup').append(editdetails);
    $('.popup').removeClass('hide');

    app_persona.v = new Vue({
        el: '.popup .editdetails',
        data: { 'persona': app_persona.persona },
        methods: {
            save: function () {
                ds_persona.save_details(app_persona.persona.id,
                                        app_persona.persona.name,
                                        app_persona.persona.designedfor,
                                        app_persona.persona.designedby,
                                        app_persona.persona.persona_when,
                                        app_persona.persona.iteration,
                                        function () {
                        window.location.hash = 'persona';
                    });
            }
        }
    });

    editdetails.find('textarea.designedfor').focus();
};
