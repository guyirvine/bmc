/*jslint browser: true*/
/*jslint nomen: true */
/*jslint node: true */

/*global $, _, jQuery, alert, console, ko, moment, guid, UUIDjs, gapi, AWS, ds_aws, ds, c3, d3, Trello, app, ds_vpc, Vue */

"use strict";

var app_vpc = {};

app_vpc.vpc_section_names = ['productsandservices', 'gaincreators', 'painrelievers',
                                 'customerjobs', 'gains', 'pains'];

app_vpc.show_openvpc = function () {
    var opencanvas;

    opencanvas = $('.templates .opencanvas').clone();
    opencanvas.find('header').text('Go to a Value Proposition Canvas');
    opencanvas.find('a.cancel').prop('href', '#vpc');
    opencanvas.find('footer a.new').prop('href', '#newvpc');

    ds_vpc.get_all(function (l) {
        console.log('ds_vpc.get_all.1 ', l);
        var ul, li;
        ul = opencanvas.find('ul');
        li = ul.find('li').remove();
        _.each(l, function (el) {
            var _li = li.clone();
            _li.find('a').text(el.name);
            _li.find('a').attr('href', '#loadvpc+' + el.id);
            ul.append(_li);
        });
    });

    $('.popup').empty();
    $('.popup').append(opencanvas);
    $('.popup').removeClass('hide');
};

app_vpc.create_vpc = function () {
    $.post('/vpc', 'New Value Proposition Canvas', function (id) {
        console.log('app_vpc.create_vpc.1.1 ', id);
        window.location.hash = '#loadvpc+' + id;
    });
};

app_vpc.load_vpc = function (id) {
    console.log('app_vpc.load_vpc.1 ', id);
    app_vpc.vpc_id = id;

    ds_vpc.get(id, function (obj) {
        localStorage.lastroute = 'loadvpc+' + id;
        console.log('app_vpc.load_vpc.1 obj: ', obj);

        _.each(app_vpc.vpc_section_names, function (section_name) {
            if (obj[section_name] !== '') {
                obj[section_name] = obj[section_name].split('\n');
            }
        });

        app_vpc.vpc = obj;
        window.location.hash = "#vpc";
    });
};

app_vpc.show_vpc = function () {
    console.log('app_vpc.show_vpc.1 ');
    $('body').removeClass('vpc');
    $('body').addClass('vpc');
    app.return_to_hash = 'vpc';

    var vpc = $('.templates .vpc').clone();
    _.each(app_vpc.vpc_section_names, function (section_name) {
        var ul = $('.templates .section-li ul').clone();

        ul.find('li').attr('v-for', 'string in vpc.' + section_name);

        vpc.find('.canvas .' + section_name)
            .append(ul)
            .attr('v-on:click', 'show_section("' + section_name + '")');
    });

    vpc.find('.details').attr('v-on:click', 'show_editdetails');

    $('.container1').empty();
    $('.container1').append(vpc);

    app_vpc.vpc_vue = new Vue({
        el: '.container1',
        data: { 'vpc': app_vpc.vpc },
        methods: {
            show_section: function (section_name) {
                window.location.hash = 'vpcsection+' + section_name;
            },
            show_editdetails: function () {
                window.location.hash = "vpceditdetails";
            }
        }
    });
};

app_vpc.show_vpc_section = function (name) {
    console.log('app_vpc.show_vpc_section.1 ', name);
    var textinput, txt;

    textinput = $('.templates .textinput').clone();
    txt = (app_vpc.vpc[name] === '') ? '' : app_vpc.vpc[name].join('\n');
    textinput.find('header').text($('.container1 .vpc .canvas .' + name + ' h2').text());
    textinput.find('.tips').append($('.templates .tips .' + name).clone());
    textinput.find('textarea').val(txt);
    textinput.find('footer a').attr('href', '#vpc');

    textinput.find('.update').on('click', function () {
        var list = textinput.find('textarea').val().split("\n");
        app_vpc.vpc[name] = _.filter(list, function (el) { return el !== ""; });
        ds_vpc.save_section(app_vpc.vpc.id, name, app_vpc.vpc[name].join('\n'), function () {
            console.log('app_vpc.show_vpc_section.1 ', name, app_vpc.vpc[name].join('\n'));
            window.location.hash = 'vpc';
        });
    });

    $('.popup').empty();
    $('.popup').append(textinput);
    $('.popup').removeClass('hide');
    textinput.find('textarea').focus();
};

app_vpc.show_editdetails = function () {
    var editdetails;

    editdetails = $('.templates .vpceditdetails').clone();

    $('.popup').empty();
    $('.popup').append(editdetails);
    $('.popup').removeClass('hide');

    app_vpc.v = new Vue({
        el: '.popup .editdetails',
        data: { 'vpc': app_vpc.vpc },
        methods: {
            save: function () {
                ds_vpc.save_details(app_vpc.vpc.id,
                                    app_vpc.vpc.name,
                                    app_vpc.vpc.designedfor,
                                    app_vpc.vpc.designedby,
                                    app_vpc.vpc.vpc_when,
                                    app_vpc.vpc.iteration,
                                    function () {
                        window.location.hash = 'vpc';
                    });
            }
        }
    });

    editdetails.find('textarea.designedfor').focus();
};
