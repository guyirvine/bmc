/*jslint browser: true*/
/*jslint nomen: true */
/*jslint node: true */

/*global $, _, jQuery, alert, console, ko, moment, UUIDjs, gapi, AWS, guid, ds_aws, ds, c3, d3, Trello, app, ds_lean, Vue, ds_assumptions */

"use strict";

var app_assumptions = {};

app_assumptions.assumptions_section_names = ['words'];

app_assumptions.show_openassumptions = function () {
    var opencanvas;

    opencanvas = $('.templates .opencanvas').clone();
    opencanvas.find('header').text('Go to a Persona Canvas');
    opencanvas.find('a.cancel').prop('href', '#lean');
    opencanvas.find('footer a.new').prop('href', '#newlean');

    ds_assumptions.get_all(function (l) {
        console.log('ds_assumptions.get_all.1 ', l);
        var ul, li;
        ul = opencanvas.find('ul');
        li = ul.find('li').remove();
        _.each(l, function (el) {
            var _li = li.clone();
            _li.find('a').text(el.name);
            _li.find('a').attr('href', '#loadlean+' + el.id);
            ul.append(_li);
        });
    });

    $('.popup').empty();
    $('.popup').append(opencanvas);
    $('.popup').removeClass('hide');
};

app_assumptions.create_assumptions = function () {
    $.post('/assumptions', 'New Assumption Canvas', function (id) {
        console.log('app_assumptions.create_assumptions.1.1 ', id);
        window.location.hash = '#loadassumptions+' + id;
    });
};

app_assumptions.load_assumptions = function (id) {
    console.log('app_assumptions.load_assumptions.1 ', id);
    app_assumptions.assumptions_id = id;

    ds_assumptions.get(id, function (obj) {
        localStorage.lastroute = 'loadassumptions+' + id;
        console.log('app_assumptions.load_assumptions.1 obj: ', obj);

        _.each(app_assumptions.assumptions_section_names, function (section_name) {
            if (obj[section_name] !== '') {
                obj[section_name] = obj[section_name].split('\n');
            }
        });

        app_assumptions.assumptions = obj;
        window.location.hash = "#assumptions";
    });
};

app_assumptions.show_assumptions = function () {
    console.log('app_assumptions.show_assumptions.1 ');
    $('body').removeClass('vpc');
    $('body').addClass('assumptions');
    app.return_to_hash = 'assumptions';

    var assumptions = $('.templates .assumptions').clone();
    _.each(app_assumptions.assumptions_section_names, function (section_name) {
        console.log('app_assumptions.show_assumptions.1 ', section_name);
        var ul = $('.templates .section-li ul').clone();

        ul.find('li').attr('v-for', 'string in assumptions.' + section_name);

        assumptions.find('.canvas .' + section_name)
            .append(ul)
            .attr('v-on:click', 'show_section("' + section_name + '")');
    });

    assumptions.find('.details').attr('v-on:click', 'show_editdetails');

    $('.container1').empty();
    $('.container1').append(assumptions);

    app_assumptions.assumptions_vue = new Vue({
        el: '.container1',
        data: { 'assumptions': app_assumptions.assumptions },
        methods: {
            show_section: function (section_name) {
                window.location.hash = 'assumptionssection+' + section_name;
            },
            show_editdetails: function () {
                window.location.hash = "assumptionseditdetails";
            }
        }
    });
};

app_assumptions.show_assumptions_section = function (name) {
    console.log('app_assumptions.show_assumptions_section.1 ', name);
    var textinput, txt;

    textinput = $('.templates .textinput').clone();
    txt = (app_assumptions.assumptions[name] === '') ? '' : app_assumptions.assumptions[name].join('\n');

    textinput.find('header').text($('.container1 .assumptions .canvas .' + name + ' h2').text());
    textinput.find('.tips').append($('.templates .leantips .' + name).clone());
    textinput.find('textarea').val(txt);
    textinput.find('a').attr('href', '#assumptions');

    textinput.find('.update').on('click', function () {
        var list = textinput.find('textarea').val().split("\n");
        app_assumptions.assumptions[name] = _.filter(list, function (el) { return el !== ""; });
        ds_assumptions.save_section(app_assumptions.assumptions.id, name, app_assumptions.assumptions[name].join('\n'), function () {
            console.log('app_assumptions.show_assumptions_section.1 ', name, app_assumptions.assumptions[name].join('\n'));
            window.location.hash = 'assumptions';
        });
    });

    $('.popup').empty();
    $('.popup').append(textinput);
    $('.popup').removeClass('hide');
    textinput.find('textarea').focus();
};

app_assumptions.show_editdetails = function () {
    var editdetails;

    editdetails = $('.templates .assumptionseditdetails').clone();
    editdetails = $('.templates .canvaseditdetails').clone();

    editdetails.find('header').text('Edit Assumptions Canvas');
    editdetails.find('footer a').attr('href', '#assumptions');

    $('.popup').empty();
    $('.popup').append(editdetails);
    $('.popup').removeClass('hide');

    app_assumptions.v = new Vue({
        el: '.popup .editdetails',
        data: { 'obj': app_assumptions.assumptions },
        methods: {
            save: function () {
                ds_assumptions.save_details(app_assumptions.assumptions.id,
                                        app_assumptions.assumptions.name,
                                        app_assumptions.assumptions.designedfor,
                                        app_assumptions.assumptions.designedby,
                                        app_assumptions.assumptions.assumptions_when,
                                        app_assumptions.assumptions.iteration,
                                        function () {
                        window.location.hash = 'assumptions';
                    });
            }
        }
    });

    editdetails.find('textarea.designedfor').focus();
};
