/*jslint browser: true*/
/*jslint nomen: true */
/*jslint node: true */

/*global $, _, jQuery, alert, console, ko, moment, UUIDjs, gapi, AWS, guid, ds_aws, ds, c3, d3, Trello, app, ds_lean, Vue, ds_questions */

"use strict";

var app_questions = {};

app_questions.questions_section_names = ['words'];

app_questions.show_openquestions = function () {
    var opencanvas;

    opencanvas = $('.templates .opencanvas').clone();
    opencanvas.find('header').text('Go to a Persona Canvas');
    opencanvas.find('a.cancel').prop('href', '#lean');
    opencanvas.find('footer a.new').prop('href', '#newlean');

    ds_questions.get_all(function (l) {
        console.log('ds_questions.get_all.1 ', l);
        var ul, li;
        ul = opencanvas.find('ul');
        li = ul.find('li').remove();
        _.each(l, function (el) {
            var _li = li.clone();
            _li.find('a').text(el.name);
            _li.find('a').attr('href', '#loadlean+' + el.id);
            ul.append(_li);
        });
    });

    $('.popup').empty();
    $('.popup').append(opencanvas);
    $('.popup').removeClass('hide');
};

app_questions.create_questions = function () {
    $.post('/questions', 'New Question Canvas', function (id) {
        console.log('app_questions.create_questions.1.1 ', id);
        window.location.hash = '#loadquestions+' + id;
    });
};

app_questions.load_questions = function (id) {
    console.log('app_questions.load_questions.1 ', id);
    app_questions.questions_id = id;

    ds_questions.get(id, function (obj) {
        localStorage.lastroute = 'loadquestions+' + id;
        console.log('app_questions.load_questions.1 obj: ', obj);

        _.each(app_questions.questions_section_names, function (section_name) {
            if (obj[section_name] !== '') {
                obj[section_name] = obj[section_name].split('\n');
            }
        });

        app_questions.questions = obj;
        window.location.hash = "#questions";
    });
};

app_questions.show_questions = function () {
    console.log('app_questions.show_questions.1 ');
    $('body').removeClass('vpc');
    $('body').addClass('questions');
    app.return_to_hash = 'questions';

    var questions = $('.templates .questions').clone();
    _.each(app_questions.questions_section_names, function (section_name) {
        console.log('app_questions.show_questions.1 ', section_name);
        var ul = $('.templates .section-li ul').clone();

        ul.find('li').attr('v-for', 'string in questions.' + section_name);

        questions.find('.canvas .' + section_name)
            .append(ul)
            .attr('v-on:click', 'show_section("' + section_name + '")');
    });

    questions.find('.details').attr('v-on:click', 'show_editdetails');

    $('.container1').empty();
    $('.container1').append(questions);

    app_questions.questions_vue = new Vue({
        el: '.container1',
        data: { 'questions': app_questions.questions },
        methods: {
            show_section: function (section_name) {
                window.location.hash = 'questionssection+' + section_name;
            },
            show_editdetails: function () {
                window.location.hash = "questionseditdetails";
            }
        }
    });
};

app_questions.show_questions_section = function (name) {
    console.log('app_questions.show_questions_section.1 ', name);
    var textinput, txt;

    textinput = $('.templates .textinput').clone();
    txt = (app_questions.questions[name] === '') ? '' : app_questions.questions[name].join('\n');

    textinput.find('header').text($('.container1 .questions .canvas .' + name + ' h2').text());
    textinput.find('.tips').append($('.templates .leantips .' + name).clone());
    textinput.find('textarea').val(txt);
    textinput.find('a').attr('href', '#questions');

    textinput.find('.update').on('click', function () {
        var list = textinput.find('textarea').val().split("\n");
        app_questions.questions[name] = _.filter(list, function (el) { return el !== ""; });
        ds_questions.save_section(app_questions.questions.id, name, app_questions.questions[name].join('\n'), function () {
            console.log('app_questions.show_questions_section.1 ', name, app_questions.questions[name].join('\n'));
            window.location.hash = 'questions';
        });
    });

    $('.popup').empty();
    $('.popup').append(textinput);
    $('.popup').removeClass('hide');
    textinput.find('textarea').focus();
};

app_questions.show_editdetails = function () {
    var editdetails;

    editdetails = $('.templates .canvaseditdetails').clone();

    editdetails.find('header').text('Edit Question Canvas');
    editdetails.find('footer a').attr('href', '#questions');

    $('.popup').empty();
    $('.popup').append(editdetails);
    $('.popup').removeClass('hide');


    app_questions.v = new Vue({
        el: '.popup .editdetails',
        data: { 'obj': app_questions.questions },
        methods: {
            save: function () {
                ds_questions.save_details(app_questions.questions.id,
                                        app_questions.questions.name,
                                        app_questions.questions.designedfor,
                                        app_questions.questions.designedby,
                                        app_questions.questions.questions_when,
                                        app_questions.questions.iteration,
                                        function () {
                        window.location.hash = 'questions';
                    });
            }
        }
    });

    editdetails.find('textarea.designedfor').focus();
};
