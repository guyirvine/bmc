/*jslint browser: true*/
/*jslint nomen: true */
/*jslint node: true */

/*global $, _, alert, console, app, AWS, ds_aws */

"use strict";

var ds_journeys = {};

ds_journeys.get = function (id, callback) {
    $.getJSON('/journeys/' + id, function (obj) {
        callback(obj);
    });
};

ds_journeys.get_all = function (callback) {
    $.getJSON('/journeys', function (l) {
        callback(l);
    });
};

ds_journeys.save = function (payload, callback) {
    $.post('/journeys/' + payload.id, JSON.stringify(payload), function (id) {
        callback(id);
    });
};

ds_journeys.save_section = function (id, name, value, callback) {
    $.post('/journeys/' + id + '/' + name, value, function () {
        callback();
    });
};

ds_journeys.save_details = function (id, name, designedfor, designedby, journey_when, iteration, callback) {
    var payload = {
        name: name,
        designedfor: designedfor,
        designedby: designedby,
        journey_when: journey_when,
        iteration: iteration
    };
    $.post('/journeys/' + id + '/details', JSON.stringify(payload), function () {
        callback();
    });
};
