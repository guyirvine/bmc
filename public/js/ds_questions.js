/*jslint browser: true*/
/*jslint nomen: true */
/*jslint node: true */

/*global $, _, alert, console, app, AWS, ds_aws */

"use strict";

var ds_questions = {};

ds_questions.get = function (id, callback) {
    $.getJSON('/questions/' + id, function (obj) {
        callback(obj);
    });
};

ds_questions.get_all = function (callback) {
    $.getJSON('/questions', function (l) {
        callback(l);
    });
};

ds_questions.save = function (payload, callback) {
    $.post('/questions/' + payload.id, JSON.stringify(payload), function (id) {
        callback(id);
    });
};

ds_questions.save_section = function (id, name, value, callback) {
    $.post('/questions/' + id + '/' + name, value, function () {
        callback();
    });
};

ds_questions.save_details = function (id, name, designedfor, designedby, questions_when, iteration, callback) {
    var payload = {
        name: name,
        designedfor: designedfor,
        designedby: designedby,
        questions_when: questions_when,
        iteration: iteration
    };
    $.post('/questions/' + id + '/details', JSON.stringify(payload), function () {
        callback();
    });
};
