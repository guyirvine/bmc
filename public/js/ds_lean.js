/*jslint browser: true*/
/*jslint nomen: true */
/*jslint node: true */

/*global $, _, alert, console, app, AWS, ds_aws */

"use strict";

var ds_lean = {};

ds_lean.get = function (id, callback) {
    $.getJSON('/lean/' + id, function (obj) {
        callback(obj);
    });
};

ds_lean.get_all = function (callback) {
    $.getJSON('/lean', function (l) {
        callback(l);
    });
};

ds_lean.save = function (payload, callback) {
    $.post('/lean/' + payload.id, JSON.stringify(payload), function (id) {
        callback(id);
    });
};

ds_lean.save_section = function (id, name, value, callback) {
    $.post('/lean/' + id + '/' + name, value, function () {
        callback();
    });
};

ds_lean.save_details = function (id, name, designedfor, designedby, lean_when, iteration, callback) {
    var payload = {
        name: name,
        designedfor: designedfor,
        designedby: designedby,
        lean_when: lean_when,
        iteration: iteration
    };
    $.post('/lean/' + id + '/details', JSON.stringify(payload), function () {
        callback();
    });
};
