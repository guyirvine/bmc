/*jslint browser: true*/
/*jslint nomen: true */
/*jslint node: true */

/*global $, _, alert, console, app, AWS, ds_aws */

"use strict";

var ds_persona = {};

ds_persona.get = function (id, callback) {
    $.getJSON('/persona/' + id, function (obj) {
        callback(obj);
    });
};

ds_persona.get_all = function (callback) {
    $.getJSON('/persona', function (l) {
        callback(l);
    });
};

ds_persona.save = function (payload, callback) {
    $.post('/persona/' + payload.id, JSON.stringify(payload), function (id) {
        callback(id);
    });
};

ds_persona.save_section = function (id, name, value, callback) {
    $.post('/persona/' + id + '/' + name, value, function () {
        callback();
    });
};

ds_persona.save_details = function (id, name, designedfor, designedby, persona_when, iteration, callback) {
    var payload = {
        name: name,
        designedfor: designedfor,
        designedby: designedby,
        persona_when: persona_when,
        iteration: iteration
    };
    $.post('/persona/' + id + '/details', JSON.stringify(payload), function () {
        callback();
    });
};
