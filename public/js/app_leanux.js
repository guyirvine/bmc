/*jslint browser: true*/
/*jslint nomen: true */
/*jslint node: true */

/*global $, _, jQuery, alert, console, ko, moment, UUIDjs, gapi, AWS, guid, ds_aws, ds, c3, d3, Trello, app, ds_lean, Vue, ds_leanux */

"use strict";

var app_leanux = {};

app_leanux.leanux_section_names = ['outcomes', 'customerneeds', 'customerneedssolved',
                                    'initialcustomers',
                                    'numberonevalue', 'additionalbenefits',
                                    'customeraquisition',
                                    'primarycompetition',
                                    'productrisk', 'productrisksolved',
                                    'otherassumptions'];

app_leanux.show_openleanux = function () {
    var opencanvas;

    opencanvas = $('.templates .opencanvas').clone();
    opencanvas.find('header').text('Go to a Lean Canvas');
    opencanvas.find('a.cancel').prop('href', '#lean');
    opencanvas.find('footer a.new').prop('href', '#newlean');

    ds_leanux.get_all(function (l) {
        console.log('ds_leanux.get_all.1 ', l);
        var ul, li;
        ul = opencanvas.find('ul');
        li = ul.find('li').remove();
        _.each(l, function (el) {
            var _li = li.clone();
            _li.find('a').text(el.name);
            _li.find('a').attr('href', '#loadlean+' + el.id);
            ul.append(_li);
        });
    });

    $('.popup').empty();
    $('.popup').append(opencanvas);
    $('.popup').removeClass('hide');
};

app_leanux.create_leanux = function () {
    $.post('/leanux', 'New LeanUX Canvas', function (id) {
        console.log('app_leanux.create_leanux.1.1 ', id);
        window.location.hash = '#loadleanux+' + id;
    });
};

app_leanux.load_leanux = function (id) {
    console.log('app_leanux.load_leanux.1 ', id);
    app_leanux.leanux_id = id;

    ds_leanux.get(id, function (obj) {
        localStorage.lastroute = 'loadleanux+' + id;
        console.log('app_leanux.load_leanux.1 obj: ', obj);

        _.each(app_leanux.leanux_section_names, function (section_name) {
            if (obj[section_name] !== '') {
                obj[section_name] = obj[section_name].split('\n');
            }
        });

        app_leanux.leanux = obj;
        window.location.hash = "#leanux";
    });
};

app_leanux.show_leanux = function () {
    console.log('app_leanux.show_leanux.1 ');
    $('body').removeClass('vpc');
    $('body').addClass('leanux');
    app.return_to_hash = 'leanux';

    var leanux = $('.templates .leanux').clone();
    _.each(app_leanux.leanux_section_names, function (section_name) {
        console.log('app_leanux.show_leanux.1 ', section_name);
        var ul = $('.templates .section-li ul').clone();

        ul.find('li').attr('v-for', 'string in leanux.' + section_name);

        leanux.find('.canvas .' + section_name)
            .append(ul)
            .attr('v-on:click', 'show_section("' + section_name + '")');
    });

    leanux.find('.details').attr('v-on:click', 'show_editdetails');

    $('.container1').empty();
    $('.container1').append(leanux);

    app_leanux.leanux_vue = new Vue({
        el: '.container1',
        data: { 'leanux': app_leanux.leanux },
        methods: {
            show_section: function (section_name) {
                window.location.hash = 'leanuxsection+' + section_name;
            },
            show_editdetails: function () {
                window.location.hash = "leanuxeditdetails";
            }
        }
    });
};

app_leanux.show_leanux_section = function (name) {
    console.log('app_leanux.show_leanux_section.1 ', name);
    var textinput, txt;

    textinput = $('.templates .textinput').clone();
    txt = (app_leanux.leanux[name] === '') ? '' : app_leanux.leanux[name].join('\n');

    textinput.find('header').text($('.container1 .leanux .canvas .' + name + ' h2').text());
    textinput.find('.tips').append($('.templates .leantips .' + name).clone());
    textinput.find('textarea').val(txt);
    textinput.find('.cancel').attr('href', '#leanux');

    textinput.find('.update').on('click', function () {
        var list = textinput.find('textarea').val().split("\n");
        app_leanux.leanux[name] = _.filter(list, function (el) { return el !== ""; });
        ds_leanux.save_section(app_leanux.leanux.id, name, app_leanux.leanux[name].join('\n'), function () {
            console.log('app_leanux.show_leanux_section.1 ', name, app_leanux.leanux[name].join('\n'));
            window.location.hash = 'leanux';
        });
    });

    $('.popup').empty();
    $('.popup').append(textinput);
    $('.popup').removeClass('hide');
    textinput.find('textarea').focus();
};

app_leanux.show_editdetails = function () {
    var editdetails;

    editdetails = $('.templates .leanuxeditdetails').clone();

    $('.popup').empty();
    $('.popup').append(editdetails);
    $('.popup').removeClass('hide');

    app_leanux.v = new Vue({
        el: '.popup .editdetails',
        data: { 'leanux': app_leanux.leanux },
        methods: {
            save: function () {
                ds_leanux.save_details(app_leanux.leanux.id,
                                    app_leanux.leanux.name,
                                    app_leanux.leanux.designedfor,
                                    app_leanux.leanux.designedby,
                                    app_leanux.leanux.leanux_when,
                                    app_leanux.leanux.iteration,
                                    function () {
                        window.location.hash = 'leanux';
                    });
            }
        }
    });

    editdetails.find('textarea.designedfor').focus();
};
