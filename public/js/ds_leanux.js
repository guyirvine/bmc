/*jslint browser: true*/
/*jslint nomen: true */
/*jslint node: true */

/*global $, _, alert, console, app, AWS, ds_aws */

"use strict";

var ds_leanux = {};

ds_leanux.get = function (id, callback) {
    $.getJSON('/leanux/' + id, function (obj) {
        callback(obj);
    });
};

ds_leanux.get_all = function (callback) {
    $.getJSON('/leanux', function (l) {
        callback(l);
    });
};

ds_leanux.save = function (payload, callback) {
    $.post('/leanux/' + payload.id, JSON.stringify(payload), function (id) {
        callback(id);
    });
};

ds_leanux.save_section = function (id, name, value, callback) {
    $.post('/leanux/' + id + '/' + name, value, function () {
        callback();
    });
};

ds_leanux.save_details = function (id, name, designedfor, designedby, leanux_when, iteration, callback) {
    var payload = {
        name: name,
        designedfor: designedfor,
        designedby: designedby,
        leanux_when: leanux_when,
        iteration: iteration
    };
    $.post('/leanux/' + id + '/details', JSON.stringify(payload), function () {
        callback();
    });
};
