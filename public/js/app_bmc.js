/*jslint browser: true*/
/*jslint nomen: true */
/*jslint node: true */

/*global $, _, jQuery, alert, console, ko, moment, UUIDjs, gapi, AWS, guid, ds_aws, ds, c3, d3, Trello, app, ds_bmc, Vue */

"use strict";

var app_bmc = {};

app_bmc.bmc_section_names = ['keypartners', 'keyactivities', 'keyresources',
                                  'valuepropositions',
                                  'customerrelationships', 'customersegments', 'channels',
                                  'coststructure', 'revenuestreams'];

app_bmc.show_openbmc = function () {
    var opencanvas;

    opencanvas = $('.templates .opencanvas').clone();
    opencanvas.find('header').text('Go to a Business Model Canvas');
    opencanvas.find('a.cancel').prop('href', '#bmc');

    ds_bmc.get_all(function (l) {
        console.log('ds_bmc.get_all.1 ', l);
        var ul, li;
        ul = opencanvas.find('ul');
        li = ul.find('li').remove();
        _.each(l, function (el) {
            var _li = li.clone();
            _li.find('a').text(el.name);
            _li.find('a').attr('href', '#loadbmc+' + el.id);
            ul.append(_li);
        });
    });

    $('.popup').empty();
    $('.popup').append(opencanvas);
    $('.popup').removeClass('hide');
};

app_bmc.create_bmc = function () {
    $.post('/bmc', 'New Business Model Canvas', function (id) {
        console.log('app_bmc.create_bmc.1.1 ', id);
        window.location.hash = '#loadbmc+' + id;
    });
};

app_bmc.load_bmc = function (id) {
    console.log('app_bmc.load_bmc.1 ', id);
    app_bmc.bmc_id = id;

    ds_bmc.get(id, function (obj) {
        localStorage.lastroute = 'loadbmc+' + id;
        console.log('app_bmc.load_bmc.1 obj: ', obj);

        _.each(app_bmc.bmc_section_names, function (section_name) {
            if (obj[section_name] !== '') {
                obj[section_name] = obj[section_name].split('\n');
            }
        });

        app_bmc.bmc = obj;
        window.location.hash = "#bmc";
    });
};

app_bmc.show_bmc = function () {
    console.log('app_bmc.show_bmc.1 ');
    $('body').removeClass('vpc');
    $('body').addClass('bmc');
    app.return_to_hash = 'bmc';

    var bmc = $('.templates .bmc').clone();
    _.each(app_bmc.bmc_section_names, function (section_name) {
        var ul = $('.templates .section-li ul').clone();

        ul.find('li').attr('v-for', 'string in bmc.' + section_name);

        bmc.find('.canvas .' + section_name)
            .append(ul)
            .attr('v-on:click', 'show_section("' + section_name + '")');
    });

    bmc.find('.details').attr('v-on:click', 'show_editdetails');

    $('.container1').empty();
    $('.container1').append(bmc);

    app_bmc.bmc_vue = new Vue({
        el: '.container1',
        data: { 'bmc': app_bmc.bmc },
        methods: {
            show_section: function (section_name) {
                window.location.hash = 'bmcsection+' + section_name;
            },
            show_editdetails: function () {
                window.location.hash = "bmceditdetails";
            }
        }
    });
};

app_bmc.show_bmc_section = function (name) {
    console.log('app_bmc.show_bmc_section.1 ', name);
    var textinput, txt;

    textinput = $('.templates .textinput').clone();
    txt = (app_bmc.bmc[name] === '') ? '' : app_bmc.bmc[name].join('\n');

    textinput.find('header').text($('.container1 .bmc .canvas .' + name + ' h2').text());
    textinput.find('.tips').append($('.templates .tips .' + name).clone());
    textinput.find('textarea').val(txt);
    textinput.find('.cancel').on('click', function () {
        window.location.hash = 'bmc';
    });

    textinput.find('.update').on('click', function () {
        var list = textinput.find('textarea').val().split("\n");
        app_bmc.bmc[name] = _.filter(list, function (el) { return el !== ""; });
        ds_bmc.save_section(app_bmc.bmc.id, name, app_bmc.bmc[name].join('\n'), function () {
            console.log('app_bmc.show_bmc_section.1 ', name, app_bmc.bmc[name].join('\n'));
            window.location.hash = 'bmc';
        });
    });

    $('.popup').empty();
    $('.popup').append(textinput);
    $('.popup').removeClass('hide');
    textinput.find('textarea').focus();
};

app_bmc.show_editdetails = function () {
    var editdetails;

    editdetails = $('.templates .bmceditdetails').clone();

    $('.popup').empty();
    $('.popup').append(editdetails);
    $('.popup').removeClass('hide');

    app_bmc.v = new Vue({
        el: '.popup .editdetails',
        data: { 'bmc': app_bmc.bmc },
        methods: {
            save: function () {
                ds_bmc.save_details(app_bmc.bmc.id,
                                    app_bmc.bmc.name,
                                    app_bmc.bmc.designedfor,
                                    app_bmc.bmc.designedby,
                                    app_bmc.bmc.bmc_when,
                                    app_bmc.bmc.iteration,
                                    function () {
                        window.location.hash = 'bmc';
                    });
            }
        }
    });

    editdetails.find('textarea.designedfor').focus();
};
