/*jslint browser: true*/
/*jslint nomen: true */
/*jslint node: true */

/*global $, _, alert, console, app, AWS, ds_aws */

"use strict";

var ds_vpc = {};

ds_vpc.get = function (id, callback) {
    $.getJSON('/vpc/' + id, function (obj) {
        callback(obj);
    });
};

ds_vpc.get_all = function (callback) {
    $.getJSON('/vpc', function (l) {
        callback(l);
    });
};

ds_vpc.save = function (payload, callback) {
    $.post('/vpc/' + payload.id, JSON.stringify(payload), function (id) {
        callback(id);
    });
};

ds_vpc.save_section = function (id, name, value, callback) {
    $.post('/vpc/' + id + '/' + name, value, function () {
        callback();
    });
};

ds_vpc.save_details = function (id, name, designedfor, designedby, vpc_when, iteration, callback) {
    var payload = {
        name: name,
        designedfor: designedfor,
        designedby: designedby,
        vpc_when: vpc_when,
        iteration: iteration
    };
    $.post('/vpc/' + id + '/details', JSON.stringify(payload), function () {
        callback();
    });
};
