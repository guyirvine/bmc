/*jslint browser: true*/
/*jslint nomen: true */
/*jslint node: true */

/*global $, _, jQuery, alert, console, ko, moment, UUIDjs, gapi, AWS, guid, ds_aws, ds, c3, d3, Trello, app, ds_lean, Vue, ds_journeys */

"use strict";

var app_journeys = {};

app_journeys.journeys_section_names = ['words'];

app_journeys.show_openjourneys = function () {
    var opencanvas;

    opencanvas = $('.templates .opencanvas').clone();
    opencanvas.find('header').text('Go to a Persona Canvas');
    opencanvas.find('a.cancel').prop('href', '#lean');
    opencanvas.find('footer a.new').prop('href', '#newlean');

    ds_journeys.get_all(function (l) {
        console.log('ds_journeys.get_all.1 ', l);
        var ul, li;
        ul = opencanvas.find('ul');
        li = ul.find('li').remove();
        _.each(l, function (el) {
            var _li = li.clone();
            _li.find('a').text(el.name);
            _li.find('a').attr('href', '#loadlean+' + el.id);
            ul.append(_li);
        });
    });

    $('.popup').empty();
    $('.popup').append(opencanvas);
    $('.popup').removeClass('hide');
};

app_journeys.create_journeys = function () {
    $.post('/journeys', 'New Journeys Canvas', function (id) {
        console.log('app_journeys.create_journeys.1.1 ', id);
        window.location.hash = '#loadjourneys+' + id;
    });
};

app_journeys.load_journeys = function (id) {
    console.log('app_journeys.load_journeys.1 ', id);
    app_journeys.journeys_id = id;

    ds_journeys.get(id, function (obj) {
        localStorage.lastroute = 'loadjourneys+' + id;
        console.log('app_journeys.load_journeys.1 obj: ', obj);

        _.each(app_journeys.journeys_section_names, function (section_name) {
            if (obj[section_name] !== '') {
                obj[section_name] = obj[section_name].split('\n');
            }
        });

        app_journeys.journeys = obj;
        window.location.hash = "#journeys";
    });
};

app_journeys.show_journeys = function () {
    console.log('app_journeys.show_journeys.1 ');
    $('body').removeClass('vpc');
    $('body').addClass('journeys');
    app.return_to_hash = 'journeys';

    var journeys = $('.templates .journeys').clone();
    _.each(app_journeys.journeys_section_names, function (section_name) {
        console.log('app_journeys.show_journeys.1 ', section_name);
        var ul = $('.templates .section-li ul').clone();

        ul.find('li').attr('v-for', 'string in journeys.' + section_name);

        journeys.find('.canvas .' + section_name)
            .append(ul)
            .attr('v-on:click', 'show_section("' + section_name + '")');
    });

    journeys.find('.details').attr('v-on:click', 'show_editdetails');

    $('.container1').empty();
    $('.container1').append(journeys);

    app_journeys.journeys_vue = new Vue({
        el: '.container1',
        data: { 'journeys': app_journeys.journeys },
        methods: {
            show_section: function (section_name) {
                window.location.hash = 'journeyssection+' + section_name;
            },
            show_editdetails: function () {
                window.location.hash = "journeyseditdetails";
            }
        }
    });
};

app_journeys.show_journeys_section = function (name) {
    console.log('app_journeys.show_journeys_section.1 ', name);
    var textinput, txt;

    textinput = $('.templates .textinput').clone();
    txt = (app_journeys.journeys[name] === '') ? '' : app_journeys.journeys[name].join('\n');

    textinput.find('header').text($('.container1 .journeys .canvas .' + name + ' h2').text());
    textinput.find('.tips').append($('.templates .leantips .' + name).clone());
    textinput.find('textarea').val(txt);
    textinput.find('.cancel').attr('href', '#journeys');

    textinput.find('.update').on('click', function () {
        var list = textinput.find('textarea').val().split("\n");
        app_journeys.journeys[name] = _.filter(list, function (el) { return el !== ""; });
        ds_journeys.save_section(app_journeys.journeys.id, name, app_journeys.journeys[name].join('\n'), function () {
            console.log('app_journeys.show_journeys_section.1 ', name, app_journeys.journeys[name].join('\n'));
            window.location.hash = 'journeys';
        });
    });

    $('.popup').empty();
    $('.popup').append(textinput);
    $('.popup').removeClass('hide');
    textinput.find('textarea').focus();
};

app_journeys.show_editdetails = function () {
    var editdetails;

    editdetails = $('.templates .canvaseditdetails').clone();

    editdetails.find('header').text('Edit Journey Canvas');
    editdetails.find('footer a').attr('href', '#journeys');

    $('.popup').empty();
    $('.popup').append(editdetails);
    $('.popup').removeClass('hide');

    app_journeys.v = new Vue({
        el: '.popup .editdetails',
        data: { 'obj': app_journeys.journeys },
        methods: {
            save: function () {
                ds_journeys.save_details(app_journeys.journeys.id,
                                        app_journeys.journeys.name,
                                        app_journeys.journeys.designedfor,
                                        app_journeys.journeys.designedby,
                                        app_journeys.journeys.journey_when,
                                        app_journeys.journeys.iteration,
                                        function () {
                        window.location.hash = 'journeys';
                    });
            }
        }
    });

    editdetails.find('textarea.designedfor').focus();
};
