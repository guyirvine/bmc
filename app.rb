require 'sinatra'
require 'sinatra/cookies'
require 'json'
require 'fluiddb2'
require 'uuidtools'

helpers do
  def user_id_for_session
    sql = 'SELECT user_id FROM bmc.open_sessions_vw WHERE key = ?'
    @db.query_for_value(sql, [cookies[:session_key]]).to_i
  end
end

before do
  @db = FluidDb2.db(ENV['DB'])
end

after do
  @db.close
end

get '/' do
  redirect '/index.htm'
end

get '/pg.htm' do
  redirect '/index.htm'
end

###############################################################################
# user
get '/user' do
  begin
    sql = 'SELECT u.id, u.username
            FROM bmc.user_tbl u
              INNER JOIN bmc.session_tbl s ON (u.id = s.user_id)
            WHERE s.key = ?'
    @db.query_for_array(sql, [cookies[:session_key]]).to_json
  rescue FluidDb2::NoDataFoundError
    404
  end
end

post '/user/vision' do
  sql = 'UPDATE bmc.user_tbl
         SET vision = ?
         WHERE id = ?'

  data = request.body.read
  payload = [data, 1]
  @db.execute(sql, payload)
end

post '/password' do
  begin
    data = JSON.parse request.body.read
    user_id = user_id_for_session

    sql = "SELECT id
           FROM bmc.password_tbl p
           WHERE 1=1
           AND p.user_id = ?
           AND p.password = crypt(?, p.password)
           AND p.current = true"

    puts "/password.1 sql: #{sql}, #{user_id}, #{data['old']}"
    password_id = @db.query_for_value(sql, [user_id, data['old']])

    sql = 'UPDATE bmc.password_tbl SET current = false WHERE id = ?'
    puts "/password.2 sql: #{sql}, #{password_id}"
    @db.execute(sql, [password_id], 1)

    sql = "INSERT INTO bmc.password_tbl(user_id,password)
            VALUES (?,crypt(?, gen_salt('md5')))"
    puts "/password.3 sql: #{user_id}, #{data['new']}"
    @db.execute(sql, [user_id, data['new']])

    puts '/password.4'
  rescue FluidDb2::NoDataFoundError
    401
  end
end

###############################################################################
# canvas
get '/canvas' do
  sql = "
          SELECT c.id, c.user_id, c.name, c.when, c.type,
                  c.name||' - '||c.type AS label
          FROM bmc.canvas_vw c
            INNER JOIN bmc.permission_vw p ON
              ( c.id = p.canvas_id AND c.type = p.type )
          WHERE p.user_id = ?
          ORDER BY c.when DESC, c.name;
          "

  @db.query_for_resultset(sql, [user_id_for_session]).to_json
end

###############################################################################
# bmc
get '/bmc' do
  sql = 'SELECT b.id, b.name
          FROM bmc.bmc_tbl b
            INNER JOIN bmc.open_sessions_vw s ON (b.user_id = s.user_id)
          WHERE s.key = ?
          ORDER BY b.createdon'
  @db.query_for_resultset(sql, [cookies[:session_key]]).to_json
end

get '/bmc/:id' do
  sql = 'SELECT b.id, b.name,
                b.designedfor, b.designedby, b.bmc_when, b.iteration,
                b.keypartners, b.keyactivities,
                b.keyresources, b.coststructure,
                b.valuepropositions, b.customerrelationships,
                b.channels, b.revenuestreams,
                b.customersegments
          FROM bmc.bmc_tbl b
          WHERE b.id = ?
          ORDER BY b.createdon'
  @db.query_for_array(sql, [params[:id]]).to_json
end

post '/bmc/:id/details' do
  sql = 'UPDATE bmc.bmc_tbl
         SET name = ?,
             designedfor = ?,
             designedby = ?,
             bmc_when = ?,
             iteration = ?
         WHERE id = ?'

  # data_string = JSON.parse request.body.read
  data_string = request.body.read
  data = JSON.parse data_string
  puts "/bmc/:id/details.1.data: #{data_string}"
  payload = [data['name'],
             data['designedfor'],
             data['designedby'],
             data['bmc_when'],
             data['iteration'],
             params[:id]]
  @db.execute(sql, payload)
end

post '/bmc/:id/:name' do
  sql = 'UPDATE bmc.bmc_tbl
         SET ' + params[:name] + ' = ?
         WHERE id = ?'

  data = request.body.read
  payload = [data,
             params[:id]]
  @db.execute(sql, payload)
end

post '/bmc' do
  sql = 'SELECT s.user_id
         FROM bmc.session_tbl s
         WHERE key = ?'
  user_id = @db.query_for_value(sql, [cookies[:session_key]])

  sql = 'INSERT INTO bmc.bmc_tbl(user_id, name)
         VALUES (?,?)'

  data = request.body.read
  payload = [user_id,
             data]
  @db.execute(sql, payload)

  @db.query_for_value("SELECT CURRVAL('bmc.bmc_seq')")
end

###############################################################################
# vpc
get '/vpc' do
  sql = 'SELECT v.id, v.name
          FROM bmc.vpc_tbl v
            INNER JOIN bmc.open_sessions_vw s ON (v.user_id = s.user_id)
          WHERE s.key = ?
          ORDER BY v.createdon'
  @db.query_for_resultset(sql, [cookies[:session_key]]).to_json
end

get '/vpc/:id' do
  sql = 'SELECT v.id, v.name,
                v.designedfor, v.designedby, v.vpc_when, v.iteration,
                v.productsandservices, v.gaincreators,
                v.painrelievers, v.customerjobs,
                v.gains, v.pains
          FROM bmc.vpc_tbl v
          WHERE v.id = ?
          ORDER BY v.createdon'
  @db.query_for_array(sql, [params[:id]]).to_json
end

post '/vpc/:id/details' do
  sql = 'UPDATE bmc.vpc_tbl
         SET name = ?,
             designedfor = ?,
             designedby = ?,
             vpc_when = ?,
             iteration = ?
         WHERE id = ?'

  # data_string = JSON.parse request.body.read
  data_string = request.body.read
  data = JSON.parse data_string
  puts "/vpc/:id/details.1.data: #{data_string}"
  payload = [data['name'],
             data['designedfor'],
             data['designedby'],
             data['vpc_when'],
             data['iteration'],
             params[:id]]
  @db.execute(sql, payload)
end

post '/vpc/:id/:name' do
  sql = 'UPDATE bmc.vpc_tbl
         SET ' + params[:name] + ' = ?
         WHERE id = ?'

  data = request.body.read
  payload = [data,
             params[:id]]
  @db.execute(sql, payload)
end

post '/vpc' do
  sql = 'SELECT s.user_id
         FROM bmc.session_tbl s
         WHERE key = ?'
  user_id = @db.query_for_value(sql, [cookies[:session_key]])

  sql = 'INSERT INTO bmc.vpc_tbl(user_id, name)
         VALUES (?,?)'

  data = request.body.read
  payload = [user_id,
             data]
  @db.execute(sql, payload)

  @db.query_for_value("SELECT CURRVAL('bmc.vpc_seq')")
end

###############################################################################
# Sessions
post '/session' do
  begin
    data = JSON.parse request.body.read

    sql = 'SELECT cp.user_id
           FROM bmc.check_password_vw cp
           WHERE 1=1
           AND cp.username = ?
           AND cp.password = crypt(?, cp.password)'
    # WHERE pswhash = crypt(?, pswhash);'
    p = [data['username'], data['password']]
    user_id = @db.query_for_value(sql, p)

    session_key = UUIDTools::UUID.random_create.to_s
    sql = 'INSERT INTO bmc.session_tbl(user_id, key, ip_address)
           VALUES (?, ?, ?);'
    @db.execute(sql, [user_id, session_key, request.ip])
    Hash['session_key', session_key].to_json

  rescue FluidDb2::NoDataFoundError
    return 401
  end
end

delete '/session' do
  sql = 'UPDATE bmc.session_tbl
         SET open = false
         WHERE key = ?
         AND ip_address = ?
         AND open = true'
  values = [request.cookies['session_key'], request.ip]
  @db.execute(sql, values)
end

###############################################################################
# lean
get '/lean' do
  sql = 'SELECT l.id, l.name
          FROM bmc.lean_tbl l
            INNER JOIN bmc.open_sessions_vw s ON (l.user_id = s.user_id)
          WHERE s.key = ?
          ORDER BY l.createdon'
  @db.query_for_resultset(sql, [cookies[:session_key]]).to_json
end

get '/lean/:id' do
  sql = 'SELECT l.id, l.name,
                l.designedfor, l.designedby, l.lean_when, l.iteration,
                l.problem, l.solution, l.keymetrics,
                l.coststructure,
                l.uniquevalueproposition, l.unfairadvantage,
                l.customersegments, l.channels,
                l.coststructure, l.revenuestreams
          FROM bmc.lean_tbl l
          WHERE l.id = ?
          ORDER BY l.createdon'
  @db.query_for_array(sql, [params[:id]]).to_json
end

post '/lean/:id/details' do
  sql = 'UPDATE bmc.lean_tbl
         SET name = ?,
             designedfor = ?,
             designedby = ?,
             lean_when = ?,
             iteration = ?
         WHERE id = ?'

  # data_string = JSON.parse request.body.read
  data_string = request.body.read
  data = JSON.parse data_string
  puts "/lean/:id/details.1.data: #{data_string}"
  payload = [data['name'],
             data['designedfor'],
             data['designedby'],
             data['lean_when'],
             data['iteration'],
             params[:id]]
  @db.execute(sql, payload)
end

post '/lean/:id/:name' do
  sql = 'UPDATE bmc.lean_tbl
         SET ' + params[:name] + ' = ?
         WHERE id = ?'

  data = request.body.read
  payload = [data,
             params[:id]]
  @db.execute(sql, payload)
end

post '/lean' do
  sql = 'SELECT s.user_id
         FROM bmc.session_tbl s
         WHERE key = ?'
  user_id = @db.query_for_value(sql, [cookies[:session_key]])

  sql = 'INSERT INTO bmc.lean_tbl(user_id, name)
         VALUES (?,?)'

  data = request.body.read
  payload = [user_id,
             data]
  @db.execute(sql, payload)

  @db.query_for_value("SELECT CURRVAL('bmc.lean_seq')")
end

###############################################################################
# leanux
get '/leanux' do
  sql = 'SELECT l.id, l.name
          FROM bmc.leanux_tbl l
            INNER JOIN bmc.open_sessions_vw s ON (l.user_id = s.user_id)
          WHERE s.key = ?
          ORDER BY l.createdon'
  @db.query_for_resultset(sql, [cookies[:session_key]]).to_json
end

get '/leanux/:id' do
  sql = 'SELECT l.id, l.name,
                l.designedfor, l.designedby, l.leanux_when, l.iteration,
                l.outcomes,
                l.customerneeds, l.customerneedssolved,
                l.initialcustomers,
                l.numberonevalue, l.additionalbenefits,
                l.customeraquisition,
                l.primarycompetition,
                l.productrisk, l.productrisksolved,
                l.otherassumptions
          FROM bmc.leanux_tbl l
          WHERE l.id = ?
          ORDER BY l.createdon'
  @db.query_for_array(sql, [params[:id]]).to_json
end

post '/leanux/:id/details' do
  sql = 'UPDATE bmc.leanux_tbl
         SET name = ?,
             designedfor = ?,
             designedby = ?,
             leanux_when = ?,
             iteration = ?
         WHERE id = ?'

  # data_string = JSON.parse request.body.read
  data_string = request.body.read
  data = JSON.parse data_string
  puts "/leanux/:id/details.1.data: #{data_string}"
  payload = [data['name'],
             data['designedfor'],
             data['designedby'],
             data['leanux_when'],
             data['iteration'],
             params[:id]]
  @db.execute(sql, payload)
end

post '/leanux/:id/:name' do
  sql = 'UPDATE bmc.leanux_tbl
         SET ' + params[:name] + ' = ?
         WHERE id = ?'

  data = request.body.read
  payload = [data,
             params[:id]]
  @db.execute(sql, payload)
end

post '/leanux' do
  sql = 'SELECT s.user_id
         FROM bmc.session_tbl s
         WHERE key = ?'
  user_id = @db.query_for_value(sql, [cookies[:session_key]])

  sql = 'INSERT INTO bmc.leanux_tbl(user_id, name)
         VALUES (?,?)'

  data = request.body.read
  payload = [user_id,
             data]
  @db.execute(sql, payload)

  @db.query_for_value("SELECT CURRVAL('bmc.leanux_seq')")
end

###############################################################################
# persona
get '/persona' do
  sql = 'SELECT l.id, l.name
          FROM bmc.persona_tbl l
            INNER JOIN bmc.open_sessions_vw s ON (l.user_id = s.user_id)
          WHERE s.key = ?
          ORDER BY l.createdon'
  @db.query_for_resultset(sql, [cookies[:session_key]]).to_json
end

get '/persona/:id' do
  sql = 'SELECT l.id, l.name,
                l.designedfor, l.designedby, l.persona_when, l.iteration,
                l.whoarethey,
                l.tasksandbehaviours,
                l.problemsandneeds,
                l.solutions
          FROM bmc.persona_tbl l
          WHERE l.id = ?
          ORDER BY l.createdon'
  @db.query_for_array(sql, [params[:id]]).to_json
end

post '/persona/:id/details' do
  sql = 'UPDATE bmc.persona_tbl
         SET name = ?,
             designedfor = ?,
             designedby = ?,
             persona_when = ?,
             iteration = ?
         WHERE id = ?'

  # data_string = JSON.parse request.body.read
  data_string = request.body.read
  data = JSON.parse data_string
  puts "/persona/:id/details.1.data: #{data_string}"
  payload = [data['name'],
             data['designedfor'],
             data['designedby'],
             data['persona_when'],
             data['iteration'],
             params[:id]]
  @db.execute(sql, payload)
end

post '/persona/:id/:name' do
  sql = 'UPDATE bmc.persona_tbl
         SET ' + params[:name] + ' = ?
         WHERE id = ?'

  data = request.body.read
  payload = [data,
             params[:id]]
  @db.execute(sql, payload)
end

post '/persona' do
  sql = 'SELECT s.user_id
         FROM bmc.session_tbl s
         WHERE key = ?'
  user_id = @db.query_for_value(sql, [cookies[:session_key]])

  sql = 'INSERT INTO bmc.persona_tbl(user_id, name)
         VALUES (?,?)'

  data = request.body.read
  payload = [user_id,
             data]
  @db.execute(sql, payload)

  @db.query_for_value("SELECT CURRVAL('bmc.persona_seq')")
end

###############################################################################
# assumptions
get '/assumptions' do
  sql = 'SELECT l.id, l.name
          FROM bmc.assumptions_tbl l
            INNER JOIN bmc.open_sessions_vw s ON (l.user_id = s.user_id)
          WHERE s.key = ?
          ORDER BY l.createdon'
  @db.query_for_resultset(sql, [cookies[:session_key]]).to_json
end

get '/assumptions/:id' do
  sql = 'SELECT l.id, l.name,
                l.designedfor, l.designedby, l.assumptions_when, l.iteration,
                l.words
          FROM bmc.assumptions_tbl l
          WHERE l.id = ?
          ORDER BY l.createdon'
  @db.query_for_array(sql, [params[:id]]).to_json
end

post '/assumptions/:id/details' do
  sql = 'UPDATE bmc.assumptions_tbl
         SET name = ?,
             designedfor = ?,
             designedby = ?,
             assumptions_when = ?,
             iteration = ?
         WHERE id = ?'

  # data_string = JSON.parse request.body.read
  data_string = request.body.read
  data = JSON.parse data_string
  puts "/assumptions/:id/details.1.data: #{data_string}"
  payload = [data['name'],
             data['designedfor'],
             data['designedby'],
             data['assumptions_when'],
             data['iteration'],
             params[:id]]
  @db.execute(sql, payload)
end

post '/assumptions/:id/:name' do
  sql = 'UPDATE bmc.assumptions_tbl
         SET ' + params[:name] + ' = ?
         WHERE id = ?'

  data = request.body.read
  payload = [data,
             params[:id]]
  @db.execute(sql, payload)
end

post '/assumptions' do
  sql = 'SELECT s.user_id
         FROM bmc.session_tbl s
         WHERE key = ?'
  user_id = @db.query_for_value(sql, [cookies[:session_key]])

  sql = 'INSERT INTO bmc.assumptions_tbl(user_id, name)
         VALUES (?,?)'

  data = request.body.read
  payload = [user_id,
             data]
  @db.execute(sql, payload)

  @db.query_for_value("SELECT CURRVAL('bmc.assumptions_seq')")
end

###############################################################################
# journeys
get '/journeys' do
  sql = 'SELECT l.id, l.name
          FROM bmc.journeys_tbl l
            INNER JOIN bmc.open_sessions_vw s ON (l.user_id = s.user_id)
          WHERE s.key = ?
          ORDER BY l.createdon'
  @db.query_for_resultset(sql, [cookies[:session_key]]).to_json
end

get '/journeys/:id' do
  sql = 'SELECT l.id, l.name,
                l.designedfor, l.designedby, l.journey_when, l.iteration,
                l.words
          FROM bmc.journeys_tbl l
          WHERE l.id = ?
          ORDER BY l.createdon'
  @db.query_for_array(sql, [params[:id]]).to_json
end

post '/journeys/:id/details' do
  sql = 'UPDATE bmc.journeys_tbl
         SET name = ?,
             designedfor = ?,
             designedby = ?,
             journey_when = ?,
             iteration = ?
         WHERE id = ?'

  # data_string = JSON.parse request.body.read
  data_string = request.body.read
  data = JSON.parse data_string
  puts "/journeys/:id/details.1.data: #{data_string}"
  payload = [data['name'],
             data['designedfor'],
             data['designedby'],
             data['journey_when'],
             data['iteration'],
             params[:id]]
  @db.execute(sql, payload)
end

post '/journeys/:id/:name' do
  sql = 'UPDATE bmc.journeys_tbl
         SET ' + params[:name] + ' = ?
         WHERE id = ?'

  data = request.body.read
  payload = [data,
             params[:id]]
  @db.execute(sql, payload)
end

post '/journeys' do
  sql = 'SELECT s.user_id
         FROM bmc.session_tbl s
         WHERE key = ?'
  user_id = @db.query_for_value(sql, [cookies[:session_key]])

  sql = 'INSERT INTO bmc.journeys_tbl(user_id, name)
         VALUES (?,?)'

  data = request.body.read
  payload = [user_id,
             data]
  @db.execute(sql, payload)

  @db.query_for_value("SELECT CURRVAL('bmc.journeys_seq')")
end

###############################################################################
# questions
get '/questions' do
  sql = 'SELECT l.id, l.name
          FROM bmc.questions_tbl l
            INNER JOIN bmc.open_sessions_vw s ON (l.user_id = s.user_id)
          WHERE s.key = ?
          ORDER BY l.createdon'
  @db.query_for_resultset(sql, [cookies[:session_key]]).to_json
end

get '/questions/:id' do
  sql = 'SELECT l.id, l.name,
                l.designedfor, l.designedby, l.questions_when, l.iteration,
                l.words
          FROM bmc.questions_tbl l
          WHERE l.id = ?
          ORDER BY l.createdon'
  @db.query_for_array(sql, [params[:id]]).to_json
end

post '/questions/:id/details' do
  sql = 'UPDATE bmc.questions_tbl
         SET name = ?,
             designedfor = ?,
             designedby = ?,
             questions_when = ?,
             iteration = ?
         WHERE id = ?'

  # data_string = JSON.parse request.body.read
  data_string = request.body.read
  data = JSON.parse data_string
  puts "/questions/:id/details.1.data: #{data_string}"
  payload = [data['name'],
             data['designedfor'],
             data['designedby'],
             data['questions_when'],
             data['iteration'],
             params[:id]]
  @db.execute(sql, payload)
end

post '/questions/:id/:name' do
  sql = 'UPDATE bmc.questions_tbl
         SET ' + params[:name] + ' = ?
         WHERE id = ?'

  data = request.body.read
  payload = [data,
             params[:id]]
  @db.execute(sql, payload)
end

post '/questions' do
  sql = 'SELECT s.user_id
         FROM bmc.session_tbl s
         WHERE key = ?'
  user_id = @db.query_for_value(sql, [cookies[:session_key]])

  sql = 'INSERT INTO bmc.questions_tbl(user_id, name)
         VALUES (?,?)'

  data = request.body.read
  payload = [user_id,
             data]
  @db.execute(sql, payload)

  @db.query_for_value("SELECT CURRVAL('bmc.questions_seq')")
end
