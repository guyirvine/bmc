CREATE SCHEMA bmc;

CREATE SEQUENCE bmc.user_seq;
CREATE SEQUENCE bmc.password_seq;
CREATE SEQUENCE bmc.session_seq;
CREATE SEQUENCE bmc.bmc_seq;
CREATE SEQUENCE bmc.vpc_seq;

CREATE TABLE bmc.user_tbl (
    id BIGINT DEFAULT NEXTVAL('bmc.user_seq') NOT NULL PRIMARY KEY,
    username VARCHAR NOT NULL
);

CREATE TABLE bmc.password_tbl (
    id BIGINT DEFAULT NEXTVAL('bmc.password_seq') NOT NULL PRIMARY KEY,
    user_id BIGINT NOT NULL REFERENCES bmc.user_tbl,
    password VARCHAR NOT NULL,
    createdon TIMESTAMP DEFAULT NOW() NOT NULL
);

CREATE TABLE bmc.session_tbl (
    id BIGINT DEFAULT NEXTVAL('bmc.session_seq') NOT NULL,
    user_id BIGINT NOT NULL REFERENCES bmc.user_tbl,
    open BOOLEAN DEFAULT TRUE NOT NULL,
    createdon TIMESTAMP DEFAULT NOW() NOT NULL,
    key VARCHAR NOT NULL,
    ip_address VARCHAR
);

CREATE TABLE bmc.bmc_tbl (
  id BIGINT DEFAULT NEXTVAL('bmc.bmc_seq') NOT NULL,
  user_id BIGINT NOT NULL REFERENCES bmc.user_tbl,
  createdon TIMESTAMP NOT NULL DEFAULT NOW(),
  name VARCHAR NOT NULL DEFAULT 'bmc',
  designedfor VARCHAR NOT NULL DEFAULT '',
  designedby VARCHAR NOT NULL DEFAULT '',
  bmc_when TIMESTAMP NOT NULL DEFAULT NOW(),
  iteration VARCHAR NOT NULL DEFAULT '1',
  keypartners VARCHAR NOT NULL DEFAULT '',
  keyactivities VARCHAR NOT NULL DEFAULT '',
  keyresources VARCHAR NOT NULL DEFAULT '',
  coststructure VARCHAR NOT NULL DEFAULT '',
  valuepropositions VARCHAR NOT NULL DEFAULT '',
  customerrelationships VARCHAR NOT NULL DEFAULT '',
  channels VARCHAR NOT NULL DEFAULT '',
  revenuestreams VARCHAR NOT NULL DEFAULT '',
  customersegments VARCHAR NOT NULL DEFAULT ''
);

CREATE TABLE bmc.vpc_tbl (
  id BIGINT DEFAULT NEXTVAL('bmc.vpc_seq') NOT NULL,
  user_id BIGINT NOT NULL REFERENCES bmc.user_tbl,
  createdon TIMESTAMP NOT NULL DEFAULT NOW(),
  name VARCHAR NOT NULL DEFAULT 'vpc',
  designedfor VARCHAR NOT NULL DEFAULT '',
  designedby VARCHAR NOT NULL DEFAULT '',
  vpc_when TIMESTAMP NOT NULL DEFAULT NOW(),
  iteration VARCHAR NOT NULL DEFAULT '1',
  productsandservices VARCHAR NOT NULL DEFAULT '',
  gaincreators VARCHAR NOT NULL DEFAULT '',
  painrelievers VARCHAR NOT NULL DEFAULT '',
  customerjobs VARCHAR NOT NULL DEFAULT '',
  gains VARCHAR NOT NULL DEFAULT '',
  pains VARCHAR NOT NULL DEFAULT ''
);

CREATE VIEW bmc.check_password_vw AS
  SELECT p1.user_id,
          u.username,
          p1.password
  FROM (bmc.user_tbl u
    INNER JOIN ( SELECT p.user_id,
                        p.password
                  FROM bmc.password_tbl p
                  ORDER BY p.createdon DESC
                  LIMIT 1) p1 ON ((u.id = p1.user_id)));
