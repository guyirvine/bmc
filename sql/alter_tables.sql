CREATE SEQUENCE bmc.journeys_seq;

CREATE TABLE bmc.journeys_tbl (
  id BIGINT NOT NULL DEFAULT NEXTVAL('bmc.journeys_seq') PRIMARY KEY,
  user_id BIGINT NOT NULL REFERENCES bmc.user_tbl,
  createdon TIMESTAMP NOT NULL DEFAULT NOW(),
  name VARCHAR NOT NULL DEFAULT 'journey',
  designedfor VARCHAR NOT NULL DEFAULT '',
  designedby VARCHAR NOT NULL DEFAULT '',
  journey_when TIMESTAMP NOT NULL DEFAULT NOW(),
  iteration VARCHAR NOT NULL DEFAULT '1',
  words VARCHAR NOT NULL DEFAULT '' );

CREATE OR REPLACE VIEW bmc.canvas_vw AS
    SELECT b.id, b.user_id, b.name, b.bmc_when AS when, 'bmc' AS type
    FROM bmc.bmc_tbl b
  UNION
    SELECT v.id, v.user_id, v.name, v.vpc_when AS when, 'vpc' AS type
    FROM bmc.vpc_tbl v
  UNION
    SELECT l.id, l.user_id, l.name, l.lean_when AS when, 'lean' AS type
    FROM bmc.lean_tbl l
  UNION
    SELECT lx.id, lx.user_id, lx.name, lx.leanux_when AS when,
            'leanux' AS type
    FROM bmc.leanux_tbl lx
  UNION
    SELECT p.id, p.user_id, p.name, p.persona_when AS when,
            'persona' AS type
    FROM bmc.persona_tbl p
  UNION
    SELECT p.id, p.user_id, p.name, p.assumptions_when AS when,
            'assumptions' AS type
    FROM bmc.assumptions_tbl p
  UNION
    SELECT p.id, p.user_id, p.name, p.journey_when AS when,
            'journeys' AS type
    FROM bmc.journeys_tbl p
  UNION
    SELECT p.id, p.user_id, p.name, p.questions_when AS when,
            'questions' AS type
    FROM bmc.questions_tbl p;
