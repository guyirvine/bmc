FROM ubuntu:16.04

MAINTAINER Guy Irvine <guy@guyirvine.com>

RUN echo "Update packages" \
  && export DEBIAN_FRONTEND=noninteractive \
  && apt-get -y update

RUN echo "Install packages 1" \
  && export DEBIAN_FRONTEND=noninteractive \
  && apt-get install -y \
      ruby \
      ruby-dev \
      git-core \
      build-essential \
      libpq-dev

RUN echo "Setup locales" \
  && localedef -c -i en_NZ -f UTF-8 en_NZ.UTF-8 \
  && update-locale LANG=en_NZ.UTF-8

RUN echo "Create user" \
  && mkdir -p /opt/bmc/ \
  && groupadd --gid 1000 puser \
  && useradd -m --home /home/puser --uid 1000 --gid puser --shell /bin/sh puser

RUN echo "Install required" \
  && gem install bundler pg fluiddb2 sinatra sinatra-contrib uuidtools

RUN echo "Cleaning up" \
  && apt-get autoremove -y \
  && apt-get clean -y \
  && rm -rf /var/lib/apt/lists/*

COPY . /opt/bmc/

#USER fpuser

WORKDIR /opt/bmc/

RUN bundle install --without test development

ENV DB='pgsql://vagrant:password@localhost/bmc'

EXPOSE 4568

ENTRYPOINT ["ruby", "app.rb", "-o", "0.0.0.0", "-p", "4568"]
